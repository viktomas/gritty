# gritty

Terminal emulator written in Go and Gio UI Framework. With focus on readability and documentation of the code.

You could potentially use this emulator for normal work, but it is mainly intended as a reference implementation of vt100-ish terminal emulator.


Run with `go run .`, test with `go test .`.
